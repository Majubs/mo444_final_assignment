# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 23:18:11 2018

@author: mjuli
"""
import numpy as np
import pandas as pd

round_stats = np.array([])
for round_no in range(1, 13):
    DATA = pd.read_csv("../input/2018/scouts_round_" + str(round_no) + ".csv").fillna(0)
    DATA["average"] = DATA["average"].apply(lambda x: float(x.replace(',', '.')))
    DATA["price"] = DATA["price"].apply(lambda x: float(x.replace(',', '.')))
    DATA["difference"] = DATA["difference"].apply(lambda x: float(x.replace(',', '.')))
    
    df = DATA.describe().loc['mean']
#    round_stats = np.append(round_stats, df.values, axis=0)
    if round_stats.size == 0:
        round_stats = np.array([df.values])
#        print(round_stats)
    else:
        round_stats = np.append(round_stats, [df.values], axis=0)
#        print(">>", len(round_stats))
    
print(round_stats)