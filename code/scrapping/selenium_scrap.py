#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 18 12:20:45 2018

@author: majubs
"""
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.common.action_chains import ActionChains

class element_has_table(object):
    def __init__(self):
        return

    def __call__(self, driver):
        print("Waiting for table to load...")
        element = driver.find_element_by_id("ctl00_cphMainContent_gvList")   # Finding the referenced element
        if len(element.find_elements_by_tag_name('tr')) == 103:
            return element
        else:
            return False

class element_has_new_table(object):
    def __init__(self, last):
        self.last =last
        return

    def __call__(self, driver):
        print("Waiting for table to load...")
        
        try:
            element = driver.find_element_by_id("ctl00_cphMainContent_gvList")   # Finding the referenced element
            print(element.find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[0].text, " <--> " ,self.last)
            if element.find_elements_by_tag_name('tr')[1].find_elements_by_tag_name('td')[0].text != self.last:
                return element
            else:
                return False
        except:
            return True

# TODOs
# 1 Iterate over table pages => use last table row to get the number
# 2 Iterate over rounds => 1 to 38
# 3 Store data in dataframes and save in files

folder = "/home/majubs/.mozilla/firefox/mwad0hks.default"
url = "https://www.cartolafcbrasil.com.br/scouts/cartola-fc-2018/rodada-"
headers = ["name", "position", "team", "situation", "price", "meatches", "average", \
           "last_score", "difference", "disarms", "goals", "assists", "no_goals", \
           "fouls_suffered", "strikes_out", "strikes_defended", "strikes_post", \
           "difficult_defense", "penalty_defense", "own_goal", "red_card", "yellow_card", \
           "goal_suffered", "penalty_lost", "fouls_commited", "offsides", "wrong_pass"]
types_status = {'glyphicon-question-sign': 1, 'pull-left': 2, 'glyphicon-ok-sign': 3, 'glyphicon-remove-sign': 4, 'glyphicon-plus-sign': 5}

# Open firefox and get url
driver = webdriver.Firefox(webdriver.firefox.firefox_profile.FirefoxProfile(folder))

# ----------------- Main Loop -----------------#
for round in range(7, 8):
    driver.get(url + str(round))
    
    # Work around to close add
    #body = driver.find_element_by_xpath('/html/body')
    #ActionChains(driver).move_to_element(body).click().perform()
    
    # Select ALL players, not only Prováveis
    select = Select(driver.find_element_by_id('ctl00_cphMainContent_drpStatus'))
    select.select_by_value('0')
    
    # Show 100 rows per page
    select2 = Select(driver.find_element_by_id('ctl00_cphMainContent_drpPageSize'))
    select2.select_by_value('100')
    
    # Filter players
    driver.find_element_by_id("ctl00_cphMainContent_btnFiltrar").click()
    #driver.implicitly_wait(30) #seconds
    wait = WebDriverWait(driver, 30)
    element = wait.until(element_has_table())
    
    # Find number of pages by counting the number of links to table pages
    n_pages = len(driver.find_element_by_id("ctl00_cphMainContent_gvList").find_elements_by_tag_name('tr')[-1].find_elements_by_tag_name('a'))
    
    scouts = []
    
    for n in range(2, n_pages + 3):
        print(">>> Page ", n-1)
    
        # Change to Beautiful Soup to do the actual scrapping [from_encoding="utf-8"]
        soup = BeautifulSoup(driver.page_source, "lxml")
        
        # Get table with data
        table = soup.find(id="ctl00_cphMainContent_gvList")
        #print(table.text)
        
        # Iterate over each row, skipping the first (header)
        rows = table.find_all('tr')
        for r in rows[1:-2]:
            cells = r.find_all('td')
            # Get name and position of player
            name, pos = cells[0].get_text().lstrip().rstrip()[:-1].split('(')
            # Get player likely to play
            situation = cells[0].find('span')['class'][-2]
            # Get player team
            team = cells[1].find('img')['alt']
            stats = []
            for c in cells[2:]:
                stats.append(c.get_text() if c.get_text() != '\xa0' else '0')
            scouts.append([name, pos, team, types_status[situation], *stats])
            print(name, pos, team, types_status[situation], stats)
        
        if n < n_pages + 2:
            driver.find_element_by_link_text(str(n)).click()
            element = wait.until(element_has_new_table(rows[1].find_all('td')[0].get_text().lstrip().rstrip()))
    
    file_name = "../../input/2018/scouts_round_" + str(round) + ".csv"
    pd.DataFrame(scouts, columns=headers).to_csv(file_name, index=False)

driver.close()