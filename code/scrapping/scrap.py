#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 14:39:34 2018

@author: majubs
"""
import urllib3 as l3
from bs4 import BeautifulSoup
from robobrowser import RoboBrowser
#from pyquery import PyQuery as pq

http = l3.PoolManager()

url = 'https://www.cartolafcbrasil.com.br/scouts/cartola-fc-2017/rodada-1'
response = http.request('GET', url)
soup = BeautifulSoup(response.data, "lxml")

all_input = soup.find_all('input', class_="btn", value="Filtrar")
for i in all_input:
    print(i, '\n')
    
select_disp = soup.find_all("select", class_="btn", id="ctl00_cphMainContent_drpStatus")
opt0 = select_disp[0].find(value="0")
opt0['selected'] = "selected"
print(opt0['selected'])

opt7 = select_disp[0].find(value="7")
del opt7['selected']

for o in select_disp[0].children:
    print(o)
    
select_n = soup.find_all("select", class_="btn", id="ctl00_cphMainContent_drpPageSize")
opt0 = select_n[0].find(value="100")
opt0['selected'] = "selected"
print(opt0['selected'])

opt7 = select_n[0].find(value="50")
del opt7['selected']

for o in select_n[0].children:
    print(o)

