#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 17 18:56:47 2018

@author: majubs
"""
#import robobrowser
from robobrowser import RoboBrowser

url = 'https://www.cartolafcbrasil.com.br/scouts/cartola-fc-2017/rodada-1'
browser = RoboBrowser(parser='lxml')
browser.open(url)

all_input = browser.find('input', class_="btn", value="Filtrar")
for i in all_input:
    print("FORM ", i, '\n')
    
select_disp = browser.find_all("select", class_="btn", id="ctl00_cphMainContent_drpStatus")
opt0 = select_disp[0].find(value="0")
opt0['selected'] = "selected"
print("Status: \n", opt0['selected'])

opt7 = select_disp[0].find(value="7")
del opt7['selected']

for o in select_disp[0].children:
    print(o)
    
select_n = browser.find_all("select", class_="btn", id="ctl00_cphMainContent_drpPageSize")
opt0 = select_n[0].find(value="100")
opt0['selected'] = "selected"
print("Page size:\n", opt0['selected'])

opt7 = select_n[0].find(value="50")
del opt7['selected']

for o in select_n[0].children:
    print(o)

table = browser.find_all("table", id="ctl00_cphMainContent_gvList")
for t in table:
    print(">>>>TABLES")
    print(t)
    
#form = browser.get_form("ctl00$cphMainContent$btnFiltrar")
#form_filter = robobrowser.forms.Form(all_input.parsed)
#print(form_filter)