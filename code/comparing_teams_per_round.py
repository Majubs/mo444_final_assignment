###########################################################
# Authors:
#    Amaury Mausbach
#    Maria Julia
#
###########################################################
# plots best possible team and best team found per round
###########################################################
# limitations:
#    calculates only for year 2017
#    consider only tactical schema 4:3:3 (TEC+GOL+2*ZAG+2*LAT+3*MEI+3*ATA)
#

import numpy as np
import pandas as pd
#import random as rd
import matplotlib.pyplot as plt
#from itertools import repeat
#from deap import base, creator, tools, algorithms

INPUT_DATA_PREFIX = "../input/2018/scouts_round_"
INPUT_BEST_TEAM_PER_ROUND = "../output/best_team_per_round_2018.csv"
INPUT_BEST_TEAM_NO_MONEY_LIMIT_V2 = "../output/v2/2018/best_teams_no_money_limit.csv"
INPUT_BEST_TEAM_NO_MONEY_LIMIT_MB = "../output/max_budget/2018/best_teams_money_limit.csv"
OUTPUT_COMPARE_PLOT = "../output/comparing_teams_2018.png"

POSITIONS = ['TEC','GOL','ZAG','LAT','MEI','ATA']
NJ = [1,1,2,2,3,3]	# number of players per position

HEADERS = ["name", "position", "team", "situation", "price", "meatches", "average", \
           "last_score", "difference", "disarms", "goals", "assists", "no_goals", \
           "fouls_suffered", "strikes_out", "strikes_defended", "strikes_post", \
           "difficult_defense", "penalty_defense", "own_goal", "red_card", "yellow_card", \
           "goal_suffered", "penalty_lost", "fouls_commited", "offsides", "wrong_pass"]

DATA = []
BTPR = []
BTPRNMLV2 = []
BTPRNMLMB = []

def qdata(round,col,val):
    return(DATA[round-1].loc[DATA[round-1][col]==val])

def qdata2(round,col1,val1,col2,val2):
    return(DATA[round-1].loc[(DATA[round-1][col1]==val1) & (DATA[round-1][col2]==val2)])

# Single player fitness
"""def eval_player(p):
    fit = 0.0
    
    if p["position"] != 'TEC': 
        fit = 1.7*p["disarms"] + 8.0*p["goals"] + 5.0*p["assists"] + 5.0*p["no_goals"] + 0.5*p["fouls_suffered"] + \
              0.7*p["strikes_out"] + 1.0*p["strikes_defended"] + 3.5*p["strikes_post"] + 3.0*p["difficult_defense"] + \
              7.0*p["penalty_defense"] - 6.0*p["own_goal"] - 5.0*p["red_card"] - 2.0*p["yellow_card"] - \
              2.0*p["goal_suffered"] - 3.5*p["penalty_lost"] - 0.5*p["fouls_commited"] - 0.5*p["offsides"] - 0.3*p["wrong_pass"]
        if p["meatches"] >= 1: # player average contribution
            fit = fit / p["meatches"]
        if p["situation"] != 3: # player not likely to play
            fit -= 1
    else: # técnico has different stats
        fit = float(p["average"].replace(',', '.'))
    return fit
"""

"""
def eval_player_pos(p):
    fit = 0.0
    
    if p["position"] != 'TEC': 
        fit = 1.7*p["disarms"] + 8.0*p["goals"] + 5.0*p["assists"] + 5.0*p["no_goals"] + 0.5*p["fouls_suffered"] + \
              0.7*p["strikes_out"] + 1.0*p["strikes_defended"] + 3.5*p["strikes_post"] + 3.0*p["difficult_defense"] + \
              7.0*p["penalty_defense"] - 6.0*p["own_goal"] - 5.0*p["red_card"] - 2.0*p["yellow_card"] - \
              2.0*p["goal_suffered"] - 3.5*p["penalty_lost"] - 0.5*p["fouls_commited"] - 0.5*p["offsides"] - 0.3*p["wrong_pass"]
        if p["meatches"] >= 1: # player average contribution
            fit = fit / p["meatches"]
    else: # técnico has different stats
        fit = float(p["average"].replace(',', '.'))
    return fit
"""


def eval_player_pos(p):
    fit = 0.0
    if p["position"] != 'TEC': 
        fit = p["last_score"] 
    else: # técnico has different stats
        fit = float(p["average"].replace(',', '.'))
    return fit


def get_last_score(round,name,team):
    print("round= {}, name= {}, team= {}".format(round,name,team))
    return DATA[round-1].loc[(DATA[round-1]["name"]==name) & (DATA[round-1]["team"]==team)]["last_score"]

# Team fitness = sum(players fitness)
def eval_team_pos(team):
    fit = 0
    print(team["last_score"])
    for i,p in team.iterrows():
        fit += eval_player_pos(p)
    return fit


def eval_player_pre(p):
    fit = get_last_score(p["round"],p["name"],p["team"]) 
    if len(fit)==0:
        return 0
    return fit.values[0]

# Team fitness = sum(players fitness)
def eval_team_pre(team):
    fit = 0
    print(team["last_score"])
    for i,p in team.iterrows():
        fit += eval_player_pre(p)
    return fit


if __name__ == "__main__":
    # Read players scouts from file
    if not DATA:
        for rnd in range(1, 13):
            DATA.append(pd.read_csv(INPUT_DATA_PREFIX + str(rnd) + ".csv"))
            print(DATA[rnd-1].shape)
    
    # Converts last_score field from str to float
    for r in range(len(DATA)):
        df=DATA[r]
        df['last_score'] = df['last_score'].apply(lambda x: float(x.replace(',', '.')))
    
    # load best team per round
    BTPR.append(pd.read_csv(INPUT_BEST_TEAM_PER_ROUND))
    print("...best team per round loaded - total {} rounds".format(int(BTPR[0].shape[0]/12)))
    
    # load best team per round no Money Limit
    BTPRNMLV2.append(pd.read_csv(INPUT_BEST_TEAM_NO_MONEY_LIMIT_V2))
    print("...best team per round no Money Limit loaded - total {} rounds".format(int(BTPRNMLV2[0].shape[0]/12)))
    
    # load best team per round Max_Budget
    BTPRNMLMB.append(pd.read_csv(INPUT_BEST_TEAM_NO_MONEY_LIMIT_MB))
    print("...best team per round Max_Budget - total {} rounds".format(int(BTPRNMLMB[0].shape[0]/12)))
    
    # calculate best team fitness
    round_stats = [[],[]]
    for r in range(1,int(BTPR[0].shape[0]/12)):
        round_stats[0].append(r+1)
        round_stats[1].append(eval_team_pos(BTPR[0].ix[r*12:r*12+11]))
    
    # calculate best team no Money Limit fitness
    round_stats.append([])
    for r in range(int(BTPRNMLV2[0].shape[0]/12)):
        round_stats[2].append(eval_team_pre(BTPRNMLV2[0].ix[r*12:r*12+11]))
    
    # calculate best team Max Budget fitness
    round_stats.append([])
    for r in range(int(BTPRNMLMB[0].shape[0]/12)):
        round_stats[3].append(eval_team_pre(BTPRNMLMB[0].ix[r*12:r*12+11]))
    
    # plot and save
    plt.plot(round_stats[0], round_stats[1], label="best possible")
    plt.plot(round_stats[0], round_stats[2], label="best no limit")
    plt.plot(round_stats[0], round_stats[3], label="best max budget")
    plt.xlabel("Round")
    plt.ylabel("Score")
    plt.legend(loc="lower right")
    plt.savefig(OUTPUT_COMPARE_PLOT, bbox_inches='tight')
    plt.show()



