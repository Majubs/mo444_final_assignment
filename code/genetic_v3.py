# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 18:15:18 2018

@author: mjuli
"""
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 22 23:35:21 2018

@author: mjulia
"""
import numpy as np
import pandas as pd
import random as rd
import matplotlib.pyplot as plt
from deap import base, creator, tools, algorithms

# DATA.shape = [pd.DataFrame(#players x 27)] x 38
DATA = []
output_file = "../output/max_budget/2018/best_teams_money_limit.csv"
headers = ["round", "fitness", "name", "position", "team", "situation", "price", "meatches", "average", \
           "last_score", "difference", "disarms", "goals", "assists", "no_goals", \
           "fouls_suffered", "strikes_out", "strikes_defended", "strikes_post", \
           "difficult_defense", "penalty_defense", "own_goal", "red_card", "yellow_card", \
           "goal_suffered", "penalty_lost", "fouls_commited", "offsides", "wrong_pass"]

def find_ind(population, individual):
    for idx, ind in enumerate(population):
        if ind == individual:
#            print(ind[0]["name"], individual[0]["name"])
            break
        
    return idx

def eaSimple_adpt(population, toolbox, max_price, cxpb, mutpb, ngen, stats=None,
             halloffame=None, verbose=__debug__):
    """This algorithm reproduce the simplest evolutionary algorithm as
    presented in chapter 7 of [Back2000]_.
    :param population: A list of individuals.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                    operators.
    :param cxpb: The probability of mating two individuals.
    :param mutpb: The probability of mutating an individual.
    :param ngen: The number of generation.
    :param stats: A :class:`~deap.tools.Statistics` object that is updated
                  inplace, optional.
    :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                       contain the best individuals, optional.
    :param verbose: Whether or not to log the statistics.
    :returns: The final population
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution
    The algorithm takes in a population and evolves it in place using the
    :meth:`varAnd` method. It returns the optimized population and a
    :class:`~deap.tools.Logbook` with the statistics of the evolution. The
    logbook will contain the generation number, the number of evalutions for
    each generation and the statistics if a :class:`~deap.tools.Statistics` is
    given as argument. The *cxpb* and *mutpb* arguments are passed to the
    :func:`varAnd` function. The pseudocode goes as follow ::
        evaluate(population)
        for g in range(ngen):
            population = select(population, len(population))
            offspring = varAnd(population, toolbox, cxpb, mutpb)
            evaluate(offspring)
            population = offspring
    As stated in the pseudocode above, the algorithm goes as follow. First, it
    evaluates the individuals with an invalid fitness. Second, it enters the
    generational loop where the selection procedure is applied to entirely
    replace the parental population. The 1:1 replacement ratio of this
    algorithm **requires** the selection procedure to be stochastic and to
    select multiple times the same individual, for example,
    :func:`~deap.tools.selTournament` and :func:`~deap.tools.selRoulette`.
    Third, it applies the :func:`varAnd` function to produce the next
    generation population. Fourth, it evaluates the new individuals and
    compute the statistics on this population. Finally, when *ngen*
    generations are done, the algorithm returns a tuple with the final
    population and a :class:`~deap.tools.Logbook` of the evolution.
    .. note::
        Using a non-stochastic selection method will result in no selection as
        the operator selects *n* individuals from a pool of *n*.
    This function expects the :meth:`toolbox.mate`, :meth:`toolbox.mutate`,
    :meth:`toolbox.select` and :meth:`toolbox.evaluate` aliases to be
    registered in the toolbox.
    .. [Back2000] Back, Fogel and Michalewicz, "Evolutionary Computation 1 :
       Basic Algorithms and Operators", 2000.
    """
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])
    print("Team maximum price: ", max_price)

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        # Remove players with price > budget
            if fit[1] > max_price:
                ind_idx = find_ind(population, ind)
#                print(ind_idx)
                del population[ind_idx]
            else:
                ind.fitness.values = fit

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(1, ngen + 1):
        # Select the next generation individuals
        offspring = toolbox.select(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            # Remove players with price > budget
            if fit[1] > max_price:
                ind_idx = find_ind(offspring, ind)
#                print(ind_idx)
                del offspring[ind_idx]
            else:
                ind.fitness.values = fit
        
        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

    return population, logbook

def select_coach(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def select_goalkeeper(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def select_back(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def select_lat(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def select_midfilder(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def select_attacker(data):
    rng = range(1, data.shape[0])
    idx = data.index[rd.choice(rng)]
    
    name = data.loc[idx]["name"]
    team = data.loc[idx]["team"]
    player = data[(data["name"] == name) & (data["team"] == team)]
            
    return player.to_dict("records")[0]

def create_individual(container, ind_data):
    # Return a single individual of type 'container'
    return container(ind_data)

def create_population(container, individuals):
    # Return a population of indivuduals that are already filled
    return container(individuals)

# Single player fitness
def eval_player(p):
    fit = 0.0
    if p:
        if p["position"] != 'TEC': 
            fit = 1.7*p["disarms"] + 8.0*p["goals"] + 5.0*p["assists"] + 5.0*p["no_goals"] + 0.5*p["fouls_suffered"] + \
                  0.7*p["strikes_out"] + 1.0*p["strikes_defended"] + 3.5*p["strikes_post"] + 3.0*p["difficult_defense"] + \
                  7.0*p["penalty_defense"] - 6.0*p["own_goal"] - 5.0*p["red_card"] - 2.0*p["yellow_card"] - \
                  2.0*p["goal_suffered"] - 3.5*p["penalty_lost"] - 0.5*p["fouls_commited"] - 0.5*p["offsides"] - 0.3*p["wrong_pass"]
            if p["meatches"] >= 1: # player average contribution
                fit = fit / p["meatches"]
            if p["situation"] != 3: # player not likely to play
                fit -= 10
        else: # técnico has different stats
            fit = float(p["average"].replace(',', '.'))
    else: # player stats not available for this round
        fit = -2
            
    return fit

# Team fitness = sum(players fitness)
def eval_team(team):
    fit = 0
    price = 0.0
    for p in team:
        count = 0
        fit += eval_player(p)
        # Penalize teams with repeated players
        for p_aux in team:
            if p == p_aux:
                count += 1
        if count > 1:
            fit -= 5
        price += float(p["price"].replace(',', '.'))
        
    return fit, price

# Select a random player with the same position and substitute
def mutation(team, indpb, data):
    # player.shape = (1 x 27 x 38)
    for pl_idx, player in enumerate(team):
        fit = eval_player(player)
        if fit <= 0: # bad performing player more likely to suffer mutation
            mul = 5
        else:
            mul = 1
        if rd.random() < indpb*mul:
            pos = player['position']
            players_pool = DATA[DATA['position'] == pos]
            while True:
                rng = range(1, players_pool.shape[0])
                idx = players_pool.index[rd.choice(rng)]
                if (player['name'] != players_pool.loc[idx]['name']) | (player['team'] != players_pool.loc[idx]['team']):
                    break
            name = players_pool.loc[idx]["name"]
            t = players_pool.loc[idx]["team"]
            new_player = players_pool.loc[(players_pool["name"] == name) & (players_pool["team"] == t)]
            team[pl_idx] = new_player.to_dict("records")[0]
            
    return team,

def run_alg(round_no, pop, max_price, cxpb, mutpb):
    hof = tools.HallOfFame(1)
    # Uses individuals fitness values to compute statistics
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("min", np.min)
    stats.register("max", np.max)
    
    # cxpb = crossover probability | mutpb = mutation probalility | ngen = number of generations
    pop, logbook = eaSimple_adpt(pop, toolbox, max_price=max_price, cxpb=cxpb, mutpb=mutpb, ngen=15, stats=stats, halloffame=hof, verbose=True)
    
    return pop, logbook, hof

if __name__ == "__main__":
#    crossover_prob = [0.5, 0.8]
#    mutation_prob = [0.2, 0.1]
#    population_size = [100, 500, 1000]
#    selection_alg = ['selTournment', 'selRandom', 'selBest', 'selNSGA2']
#    grid = []
#    for alg in selection_alg:
#        for size in population_size:
#            for cross in crossover_prob:
#                for mut in mutation_prob:
#                    grid.append((cross, mut, size, alg))
#    print(pd.DataFrame(grid))
    
    DATA = pd.read_csv("../input/2018/scouts_round_1.csv").fillna(0)
    
    # Define fitness function and what is an individual
    # weights=(1.0) => maximize a single objetive function (fitness can be any real number)
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    
    toolbox = base.Toolbox()
    toolbox.register("coach", select_coach, DATA[DATA['position'] == 'TEC'])
    toolbox.register("goalkeeper", select_goalkeeper, DATA[DATA['position'] == 'GOL'])
    toolbox.register("back", select_back, DATA[DATA['position'] == 'ZAG'])
    toolbox.register("lat", select_lat, DATA[DATA['position'] == 'LAT'])
    toolbox.register("midfilder", select_midfilder, DATA[DATA['position'] == 'MEI'])
    toolbox.register("attacker", select_attacker, DATA[DATA['position'] == 'ATA'])
    # Function to create an individual
    team_433 = [toolbox.coach, toolbox.goalkeeper, toolbox.back, toolbox.back, toolbox.lat, toolbox.lat, toolbox.midfilder, \
                toolbox.midfilder, toolbox.midfilder, toolbox.attacker, toolbox.attacker, toolbox.attacker]
    toolbox.register("individual", tools.initCycle, creator.Individual, team_433, n=1)
    # Function to create a population
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    
    # Genetic Operators
    toolbox.register("evaluate", eval_team)
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", mutation, indpb=0.10, data=DATA) #indpb=> probability of mutation to each attribute
    # Choose selection model
    toolbox.register("select", tools.selTournament, tournsize=5)
    
    # Initial population for first round
    pop = toolbox.population(n=1000) 
    max_price = 100.0
    
    max_prices = [max_price]
    round_stats = np.array([])
    ind_stats = []
#    hofs = []
    for round_no in range(2, 13):
        print("Running for round: ", round_no)
        
        # Run genetic algorithm
        new_pop, log, hof = run_alg(round_no, pop, max_price, 0.4, 0.2)
        price = 0.0
        for i, p in enumerate(hof[0]):
            if p:
                price += float(p["price"].replace(',', '.'))
                print(">>>", i, p['name'], p['position'], p['team'], p['average'], p['price'])
                stats = [round_no, hof[0].fitness.values[0]] + list(p.values())
                ind_stats.append(stats)
            else:
                print(">>>", i, "No player stats!")   
        print("\nWith fitness: %s and total price: %.2f" % (hof[0].fitness, price))
        
        # Plot and save results
#        hofs.append((price, hof[0]))
        gen, avg, min_, max_ = log.select("gen", "avg", "min", "max")
        plt.plot(gen, avg, label="average")
        plt.plot(gen, min_, label="minimum")
        plt.plot(gen, max_, label="maximum")
        plt.xlabel("Generation")
        plt.ylabel("Fitness")
        plt.legend(loc="lower right")
        plt.savefig("../output/max_budget/2018/evolution_" + str(round_no) + ".png", bbox_inches='tight')
        plt.show()
        
        # Use last population of previous round as starting population for next round
        ind_data = []
        pop = []
        toolbox.register("individual_from_data", create_individual, creator.Individual, ind_data)
#        toolbox.register("new_population", create_population, list, pop_list)
        if round_no not in [10, 27, 28, 33]:
            DATA = pd.read_csv("../input/2017/scouts_round_" + str(round_no) + ".csv").fillna(0)
        for ind in new_pop:
            ind_data = []
            for player in ind:
                p = DATA[(DATA["name"] == player["name"]) & (DATA["team"] == player["team"])]
                if p.empty:
                    p = player
                    p["meatches"] = p["disarms"] = p["goals"] = p["assists"] = p["no_goals"] = 0
                    p["fouls_suffered"] = p["strikes_out"] = p["strikes_defended"] = p["strikes_post"] = 0
                    p["difficult_defense"] = p["penalty_defense"] =  p["own_goal"] = p["red_card"] = p["yellow_card"] = 0
                    p["goal_suffered"] = p["penalty_lost"] = p["fouls_commited"] = p["offsides"] = p["wrong_pass"] = 0
                    p["average"] = p["last_score"] = p["difference"] = '0,0'
                    ind_data.append(p)
                else:
                    ind_data.append(p.to_dict("records")[0])
            new_ind = creator.Individual(ind_data)
#            new_ind = toolbox.individual_from_data()
            pop.append(new_ind)
        
        # Complete the population
        pop = pop + toolbox.population(n=(1000-len(pop)))
        print("New starting population created")
        
        # Get new max_price for next generation
        new_price = 0.0
        for p in hof[0]:
            new_p = DATA[(DATA["name"] == p["name"]) & (DATA["team"] == p["team"])]
            if new_p.empty:
                new_price += float(p["price"].replace(',', '.'))
            else:
                new_price += float(new_p["price"].values[0].replace(',', '.'))
        max_price = new_price
        if round_no < 12:
            max_prices.append(new_price)
            
        # Save and plot round statistics
        if round_stats.size == 0:
            round_stats = np.array([np.array([round_no, hof[0].fitness.values[0], price])])
            print(round_stats)
        else:
            round_stats = np.append(round_stats, [np.array([round_no, hof[0].fitness.values[0], price])], axis=0)
            print(">>", len(round_stats))
        # End FOR rounds
    
    plt.plot(round_stats[:, 0], round_stats[:, 1], label="fitness")
    plt.plot(round_stats[:, 0], round_stats[:, 2], label="price")
    plt.plot(round_stats[:, 0], max_prices, label="max price")
    plt.xlabel("Round")
    plt.ylabel("Values")
    plt.legend(loc="lower left")
    plt.savefig("../output/max_budget/2018/all_results_plot.png", bbox_inches='tight')
    plt.show()
    
    pd.DataFrame(round_stats).to_csv("../output/max_budget/2018/all_results.csv", header=['round_no', 'avg_fitness', 'price'], index=False, mode='w')
    
    # Save best team from all rounds to csv
    pd.DataFrame(ind_stats).to_csv(output_file, header=headers, index=False, mode='w')
    
    #Add header to output file
#    df = pd.read_csv(output_file + str(round_no) + ".csv", names=headers)
#    df.to_csv(output_file + str(round_no) + ".csv", index=False)
