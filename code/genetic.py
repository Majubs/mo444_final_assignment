#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 17:24:24 2018

http://deap.readthedocs.io/en/master
https://github.com/DEAP/deap

Hyperparameters: population size, cxpb, mutpb, select algorithm

@author: majubs
"""
import os.path as osp
import numpy as np
import pandas as pd
import random as rd
import matplotlib.pyplot as plt
from deap import base, creator, tools, algorithms

# DATA.shape = [pd.DataFrame(#players x 27)] x 38
DATA = []
file = "../output/grid_search/best_indiviuals_no_money_limit_"
output_file = file
headers = ["round", "fitness", "name", "position", "team", "situation", "price", "meatches", "average", \
           "last_score", "difference", "disarms", "goals", "assists", "no_goals", \
           "fouls_suffered", "strikes_out", "strikes_defended", "strikes_post", \
           "difficult_defense", "penalty_defense", "own_goal", "red_card", "yellow_card", \
           "goal_suffered", "penalty_lost", "fouls_commited", "offsides", "wrong_pass"]
glob_rnd = 0

def save_best_ind(fitness, indvidual):
    ind_rnd = []
    if osp.isfile(output_file):
        cur_round = glob_rnd + 1
    else:
        cur_round = glob_rnd
    # Get player stats for current round
    for player in indvidual:
        if player[glob_rnd]:
            stats = [cur_round, fitness] + list(player[glob_rnd].values())
            ind_rnd.append(stats)
        else:
            stats = [cur_round, fitness] + list(player[0].values())
            ind_rnd.append(stats)
#    print("> ", len(ind_rnd), len(ind_rnd[0]))
    pd.DataFrame(ind_rnd).to_csv(output_file, header=None, index=False, mode='a')
    
def get_best_individual(population):
    max_fit = 0.0
    max_ind = population[0]
    # Find best indiviudal of current population
    for ind in population:
#        print(type(ind.fitness.values[0]), ind.fitness.values[0], max_fit)
        if ind.fitness.values[0] > max_fit:
            max_fit = ind.fitness.values[0]
            max_ind = ind
            
    return max_fit, max_ind
            

def eaSimple_adpt(population, toolbox, cxpb, mutpb, ngen, stats=None,
             halloffame=None, verbose=__debug__):
    """This algorithm reproduce the simplest evolutionary algorithm as
    presented in chapter 7 of [Back2000]_.
    :param population: A list of individuals.
    :param toolbox: A :class:`~deap.base.Toolbox` that contains the evolution
                    operators.
    :param cxpb: The probability of mating two individuals.
    :param mutpb: The probability of mutating an individual.
    :param ngen: The number of generation.
    :param stats: A :class:`~deap.tools.Statistics` object that is updated
                  inplace, optional.
    :param halloffame: A :class:`~deap.tools.HallOfFame` object that will
                       contain the best individuals, optional.
    :param verbose: Whether or not to log the statistics.
    :returns: The final population
    :returns: A class:`~deap.tools.Logbook` with the statistics of the
              evolution
    The algorithm takes in a population and evolves it in place using the
    :meth:`varAnd` method. It returns the optimized population and a
    :class:`~deap.tools.Logbook` with the statistics of the evolution. The
    logbook will contain the generation number, the number of evalutions for
    each generation and the statistics if a :class:`~deap.tools.Statistics` is
    given as argument. The *cxpb* and *mutpb* arguments are passed to the
    :func:`varAnd` function. The pseudocode goes as follow ::
        evaluate(population)
        for g in range(ngen):
            population = select(population, len(population))
            offspring = varAnd(population, toolbox, cxpb, mutpb)
            evaluate(offspring)
            population = offspring
    As stated in the pseudocode above, the algorithm goes as follow. First, it
    evaluates the individuals with an invalid fitness. Second, it enters the
    generational loop where the selection procedure is applied to entirely
    replace the parental population. The 1:1 replacement ratio of this
    algorithm **requires** the selection procedure to be stochastic and to
    select multiple times the same individual, for example,
    :func:`~deap.tools.selTournament` and :func:`~deap.tools.selRoulette`.
    Third, it applies the :func:`varAnd` function to produce the next
    generation population. Fourth, it evaluates the new individuals and
    compute the statistics on this population. Finally, when *ngen*
    generations are done, the algorithm returns a tuple with the final
    population and a :class:`~deap.tools.Logbook` of the evolution.
    .. note::
        Using a non-stochastic selection method will result in no selection as
        the operator selects *n* individuals from a pool of *n*.
    This function expects the :meth:`toolbox.mate`, :meth:`toolbox.mutate`,
    :meth:`toolbox.select` and :meth:`toolbox.evaluate` aliases to be
    registered in the toolbox.
    .. [Back2000] Back, Fogel and Michalewicz, "Evolutionary Computation 1 :
       Basic Algorithms and Operators", 2000.
    """
    global glob_rnd
    logbook = tools.Logbook()
    logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    # Evaluate the individuals with an invalid fitness
    invalid_ind = [ind for ind in population if not ind.fitness.valid]
    fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
    for ind, fit in zip(invalid_ind, fitnesses):
        ind.fitness.values = fit

    # Find best indiviudal of round 0
    max_fit, max_ind = get_best_individual(population)
    save_best_ind(max_fit, max_ind)
#    print("Initial population: ", max_fit)

    if halloffame is not None:
        halloffame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(invalid_ind), **record)
    if verbose:
        print(logbook.stream)

    # Begin the generational process
    for gen in range(1, ngen + 1):
        glob_rnd = gen - 1
        # Select the next generation individuals
        offspring = toolbox.select(population, len(population))

        # Vary the pool of individuals
        offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        # Find best indiviudal of current population
        max_fit, max_ind = get_best_individual(offspring)
        save_best_ind(max_fit, max_ind)
        
        # Update the hall of fame with the generated individuals
        if halloffame is not None:
            halloffame.update(offspring)

        # Replace the current population by the offspring
        population[:] = offspring

        # Append the current generation statistics to the logbook
        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalid_ind), **record)
        if verbose:
            print(logbook.stream)

    return population, logbook

def select_coach(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

def select_goalkeeper(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

def select_back(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

def select_lat(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

def select_midfilder(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

def select_attacker(data):
    rng = range(1, data[0].shape[0])
    idx = data[0].index[rd.choice(rng)]
    
    name = data[0].loc[idx]["name"]
    team = data[0].loc[idx]["team"]
    player = []
    # Get player scouts for 38 rounds
    for rnd in range(0, 38):
        rnd_data = data[rnd].loc[(data[rnd]["name"] == name) & (data[rnd]["team"] == team)]
#        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
        if not rnd_data.empty:
            player.append(rnd_data.to_dict("records")[0])
        else:
            player.append({})
            
    return player

# Single player fitness
def eval_player(p):
    fit = 0
    if p[glob_rnd]:
        if p[glob_rnd]["position"] != 'TEC': 
            fit = 1.7*p[glob_rnd]["disarms"] + 8.0*p[glob_rnd]["goals"] + 5.0*p[glob_rnd]["assists"] + \
                5.0*p[glob_rnd]["no_goals"] + 0.5*p[glob_rnd]["fouls_suffered"] + 0.7*p[glob_rnd]["strikes_out"] + \
                1.0*p[glob_rnd]["strikes_defended"] + 3.5*p[glob_rnd]["strikes_post"] + 3.0*p[glob_rnd]["difficult_defense"] + \
                7.0*p[glob_rnd]["penalty_defense"] - 6.0*p[glob_rnd]["own_goal"] - 5.0*p[glob_rnd]["red_card"] - \
                2.0*p[glob_rnd]["yellow_card"] - 2.0*p[glob_rnd]["goal_suffered"] - 3.5*p[glob_rnd]["penalty_lost"] - \
                0.5*p[glob_rnd]["fouls_commited"] - 0.5*p[glob_rnd]["offsides"] - 0.3*p[glob_rnd]["wrong_pass"]
            if p[glob_rnd]["meatches"] >= 1: # player average contribution
                fit = fit / p[glob_rnd]["meatches"]
            if p[glob_rnd]["situation"] != 3: # player not likely to play
                fit -= 10
        else: # técnico has different stats
            fit = float(p[glob_rnd]["average"].replace(',', '.'))
    else: # player stats not available for this round
        fit = -2
            
    return fit

# Team fitness = sum(players fitness)
def eval_team(team):
    fit = 0
    #print(glob_rnd)
    for p in team:
        count = 0
        fit += eval_player(p)
        # Penalize teams with repeated players
        for p_aux in team:
            if p == p_aux:
                count += 1
        if count > 1:
            fit -= 5
        
    return fit, 

# Select a random player with the same position and substitute
def mutation(team, indpb, data):
    # player.shape = (1 x 27 x 38)
    for pl_idx, player in enumerate(team):
        fit = eval_player(player)
        if fit <= 0: # bad performing player more likely to suffer mutation
            mul = 5
        else:
            mul = 1
        if rd.random() < indpb*mul:
            pos = player[0]['position']
            players_pool = [p[p['position'] == pos] for p in DATA]
            while True:
                rng = range(1, players_pool[0].shape[0])
                idx = players_pool[0].index[rd.choice(rng)]
                if (player[0]['name'] != players_pool[0].loc[idx]['name']) | (player[0]['team'] != players_pool[0].loc[idx]['team']):
                    break
            name = players_pool[0].loc[idx]["name"]
            t = players_pool[0].loc[idx]["team"]
            new_player = []
            # Get player scouts for 38 rounds
            for rnd in range(0, 38):
                rnd_data = players_pool[rnd].loc[(players_pool[rnd]["name"] == name) & (players_pool[rnd]["team"] == t)]
        #        print(rnd, type(rnd_data), "\n", rnd_data.to_dict("records"))
                if not rnd_data.empty:
                    new_player.append(rnd_data.to_dict("records")[0])
                else:
                    new_player.append({})
            team[pl_idx] = new_player
            
    return team,

def run_alg(cxpb, mutpb, popsize):
    pop = toolbox.population(n=popsize)
    hof = tools.HallOfFame(1)
    # Uses individuals fitness values to compute statistics
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("min", np.min)
    stats.register("max", np.max)
    
    # cxpb = crossover probability | mutpb = mutation probalility | ngen = number of generations
    pop, logbook = eaSimple_adpt(pop, toolbox, cxpb=cxpb, mutpb=mutpb, ngen=37, stats=stats, halloffame=hof, verbose=True)
    
    #Add header to output file
    df = pd.read_csv(output_file, names=headers)
    df.to_csv(output_file, index=False)
    
    return pop, logbook, hof

if __name__ == "__main__":
    crossover_prob = [0.5, 0.8]
    mutation_prob = [0.2, 0.1]
    population_size = [100, 500, 1000]
    selection_alg = ['selTournment', 'selRandom', 'selBest', 'selNSGA2']
    grid = []
    for alg in selection_alg:
        for size in population_size:
            for cross in crossover_prob:
                for mut in mutation_prob:
                    grid.append((cross, mut, size, alg))
    print(pd.DataFrame(grid))
    
     # Read players scouts from files
    if not DATA:
        for rnd in range(1, 39):
            # TODO apply OHE to categorical columns, maybe
            DATA.append(pd.read_csv("../input/2017/scouts_round_" + str(rnd) + ".csv").fillna(0))
    
    stats = np.array([])
    for p_no, params in enumerate(grid):
        hofs = []
        print("Running for parameters: ", params)
        
        for run in range(5):
            print("Run number ", run)
            output_file = file + str(p_no) + "_" + str(run) + ".csv"
            glob_rnd = 0
            
            # Define fitness function and what is an individual
            # weights=(1.0) => maximize a single objetive function (fitness can be any real number)
            creator.create("FitnessMax", base.Fitness, weights=(1.0,))
            creator.create("Individual", list, fitness=creator.FitnessMax)
            
            toolbox = base.Toolbox()
            toolbox.register("coach", select_coach, [p[p['position'] == 'TEC'] for p in DATA])
            toolbox.register("goalkeeper", select_goalkeeper, [p[p['position'] == 'GOL'] for p in DATA])
            toolbox.register("back", select_back, [p[p['position'] == 'ZAG'] for p in DATA])
            toolbox.register("lat", select_lat, [p[p['position'] == 'LAT'] for p in DATA])
            toolbox.register("midfilder", select_midfilder, [p[p['position'] == 'MEI'] for p in DATA])
            toolbox.register("attacker", select_attacker, [p[p['position'] == 'ATA'] for p in DATA])
            # Function to create an individual
            team_433 = [toolbox.coach, toolbox.goalkeeper, toolbox.back, toolbox.back, toolbox.lat, toolbox.lat, toolbox.midfilder, \
                        toolbox.midfilder, toolbox.midfilder, toolbox.attacker, toolbox.attacker, toolbox.attacker]
            toolbox.register("individual", tools.initCycle, creator.Individual, team_433, n=1)
            # Function to create a population
            toolbox.register("population", tools.initRepeat, list, toolbox.individual)
            
            # Genetic Operators
            toolbox.register("evaluate", eval_team)
            toolbox.register("mate", tools.cxTwoPoint)
            toolbox.register("mutate", mutation, indpb=0.10, data=DATA) #indpb=> probability of mutation to each attribute
            # Choose selection model
            if params[3] == 'selNSGA2':
                toolbox.register("select", tools.selNSGA2)
            elif params[3] == 'selRandom':
                toolbox.register("select", tools.selRandom)
            elif params[3] == 'selBest':
                toolbox.register("select", tools.selBest)
            else:
                toolbox.register("select", tools.selTournament, tournsize=5)
            
            # Run genetic algorithm
            pop, log, hof = run_alg(params[0], params[1], params[2])
            price = 0.0
            for i, p in enumerate(hof[0]):
                if p[37]:
                    price += float(p[37]["price"].replace(',', '.'))
                    print(">>>", i, p[37]['name'], p[37]['position'], p[37]['team'], p[37]['average'], p[37]['price'])
                else:
                    price += float(p[0]["price"].replace(',', '.'))
                    print(">>>", i, p[0]['name'], p[0]['position'], p[0]['team'], p[0]['average'], p[0]['price'])   
            print("\nWith fitness: %s and total price: %.2f" % (hof[0].fitness, price))
            
            # Plot and save results
            hofs.append((price, hof))
            gen, avg, min_, max_ = log.select("gen", "avg", "min", "max")
            plt.plot(gen, avg, label="average")
            plt.plot(gen, min_, label="minimum")
            plt.plot(gen, max_, label="maximum")
            plt.xlabel("Generation")
            plt.ylabel("Fitness")
            plt.legend(loc="lower right")
            plt.savefig("../output/grid_search/evolution_" + str(p_no) + "_" + str(run) + ".png", bbox_inches='tight')
            plt.show()
#            plt.close()
        # End for run
        
        best_idx = idx = 0
        fits = []
        prices = []
        for price, hof in hofs:
            fits.append(hof[0].fitness.values[0])
            prices.append(price)
            if hofs[best_idx][1][0].fitness.values[0] < hof[0].fitness.values[0]:
                best_idx = idx
            idx += 1
        
        print(p_no, sum(fits)/5, sum(prices)/5, best_idx)
        if stats.size == 0:
            stats = np.array([np.array([p_no, best_idx, sum(fits)/5, max(fits), min(fits), sum(prices)/5, max(prices), min(prices)])])
            print(stats)
        else:
            stats = np.append(stats, [np.array([p_no, best_idx, sum(fits)/5, max(fits), min(fits), sum(prices)/5, max(prices), min(prices)])], axis=0)
            print(len(stats))
    # End for params
    
    plt.plot(stats[:, 0], stats[:, 1], label="avg fitness")
    plt.plot(stats[:, 0], stats[:, 2], label="avg price")
    plt.xlabel("Grid Search Params")
    plt.ylabel("Value")
    plt.legend(loc="lower right")
    plt.savefig("../output/grid_search/grid_search_results_plot.png", bbox_inches='tight')
    plt.show()
#    plt.close()
    pd.DataFrame(stats).to_csv("../output/grid_search_results.csv", header=['param_no', 'best_idx', 'avg_fitness', 'max_fitness', \
                                'min_fitness', 'avg_price', 'max_price', 'min_price'], index=False, mode='w')