###########################################################
# Authors:
#    Amaury Mausbach
#    Maria Julia
#
###########################################################
# calculates the best team per round and write to file
###########################################################
# limitations:
#    calculates only for year 2017
#    consider only tactical schema 4:3:3 (TEC+GOL+2*ZAG+2*LAT+3*MEI+3*ATA)
#

#import numpy as np
import pandas as pd
#import random as rd
#import matplotlib.pyplot as plt
#from itertools import repeat
#from deap import base, creator, tools, algorithms

OUTPUT_FILE = '../output/best_team_per_round_2018.csv'
POSITIONS = ['TEC','GOL','ZAG','LAT','MEI','ATA']
NJ = [1,1,2,2,3,3]	# number of players per position

HEADERS = ["name", "position", "team", "situation", "price", "meatches", "average", \
           "last_score", "difference", "disarms", "goals", "assists", "no_goals", \
           "fouls_suffered", "strikes_out", "strikes_defended", "strikes_post", \
           "difficult_defense", "penalty_defense", "own_goal", "red_card", "yellow_card", \
           "goal_suffered", "penalty_lost", "fouls_commited", "offsides", "wrong_pass"]

DATA = []

if __name__ == "__main__":
    # Read players scouts from file
    if not DATA:
        for rnd in range(1, 13):
            DATA.append(pd.read_csv("../input/2018/scouts_round_" + str(rnd) + ".csv"))
            print(DATA[rnd-1].shape)
    
    # Converts last_score field from str to float
    for r in range(len(DATA)):
        df=DATA[r]
        df['last_score'] = df['last_score'].apply(lambda x: float(x.replace(',', '.')))
    
    # Find best players per round aligned tatical schema and write to file
    sdf=df.head(0)
    print("writing best players per rount to file = {}".format(OUTPUT_FILE))
    sdf.to_csv(OUTPUT_FILE, header=HEADERS, index=False, mode='w')
    for r in range(len(DATA)):
        print("   writing round {}".format(r+1))
        df=DATA[r]
        for nj,pos in zip(NJ,POSITIONS):
            sdf=df.loc[df['position']== pos].nlargest(nj,'last_score')
            sdf.to_csv(OUTPUT_FILE, header=None, index=False, mode='a')

